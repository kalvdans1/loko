# Bochs Graphics demo for Loko

This program demonstrates how to compile and run a program.

The program uses the Bochs Graphics Adapter (BGA) to enable graphics
and draw to the screen. For simplicity, the banked mode is used.
